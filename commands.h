#ifndef _COMMANDS_H_
#define _COMMANDS_H_

#define CMDLINE_MAX_ARGS 4

extern int CMD_test(int argc, char** argv);
extern int CMD_led(int argc, char** argv);
extern int CMD_gpio(int argc, char** argv);

#define PORTA_PIN GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | \
                  GPIO_PIN_6 | GPIO_PIN_7

#define PORTB_PIN GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | \
                  GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7

#define PORTC_PIN GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | \
                  GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7

#define PORTD_PIN GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | \
                  GPIO_PIN_6 | GPIO_PIN_7

#define PORTE_PIN GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | \
                  GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7

#define PORTF_PIN GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | \
                  GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7

#endif 
