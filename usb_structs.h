#ifndef _USB_STRUCTS_H_
#define _USB_STRUCTS_H

#define BULK_BUFFER_SIZE 256

extern unsigned long receive_handler(void *internal, unsigned long event,
                                     unsigned long msg_value, void *msg_data);
extern unsigned long transmit_handler(void *internal, unsigned long event,
                                     unsigned long msg_value, void *msg_data);


unsigned long USBDKOKOPacketRead(void *instance, unsigned char *data,
                   unsigned long length, tBoolean last_call);

extern const tUSBBuffer usb_transmit_buffer;
extern const tUSBBuffer usb_receive_buffer;
extern const tUSBDBulkDevice usb_bulk_device;
extern unsigned char usb_transmit_ring_buffer[];
extern unsigned char usb_receive_ring_buffer[];

#endif
