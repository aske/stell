#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "utils/uartstdio.h"

#include "inc/hw_gpio.h"
#include "inc/hw_uart.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "driverlib/systick.h"
#include "driverlib/uart.h"
#include "driverlib/timer.h"

#include "driverlib/usb.h"
#include "usblib/usblib.h"
#include "usblib/usbcdc.h"
#include "usblib/usb-ids.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdbulk.h"
#include "utils/ustdlib.h"

#include "utils/cmdline.h"

#include "buttons.h"

#include "usb_structs.h"
#include "commands.h"
#include "utilities.h"

#ifdef DEBUG
void __error__(char* filename, unsigned long line) {}
#endif

#define INPUT_BUFFER_SIZE 128

static char input_buffer[INPUT_BUFFER_SIZE];
static int input_buffer_limit;

#define SYSTICKS_PER_SECOND 100
#define SYSTICK_PERIOD_MS (1000 / SYSTICKS_PER_SECOND)
#define BUTTON_POLL_DIVIDER 12

volatile unsigned long systick_count = 0;

static volatile tBoolean usb_configured = false;

static unsigned long buttons_state = 0;

static void send_message(unsigned char *message, unsigned long msg_length) {
  tUSBRingBufObject transmit_ring;
  USBBufferInfoGet(&usb_transmit_buffer, &transmit_ring);
  
  unsigned long space_left = USBBufferSpaceAvailable(&usb_transmit_buffer);
  unsigned long write_index = transmit_ring.ulWriteIndex;

  unsigned long write_count = (space_left < msg_length) ? space_left : msg_length;

  for (int i = 0; i < write_count; ++i) {
    usb_transmit_ring_buffer[write_index] = message[i];
    UARTprintf("ms[%d] = %d | %d\n", i, message[i], usb_transmit_ring_buffer[write_index]);
    ++write_index;
    write_index = (write_index == BULK_BUFFER_SIZE) ? 0 : write_index;
  }
  USBBufferDataWritten(&usb_transmit_buffer, write_count);
  /* unsigned long written = USBBufferWrite((tUSBBuffer*)&usb_transmit_buffer, "aaaaa", 5); */
}

void systick_int_handler(void) {
  ++systick_count;
  buttons_state = ButtonsPoll(0, 0);

  switch (buttons_state & ALL_BUTTONS) {
    case LEFT_BUTTON:
      if ((systick_count % BUTTON_POLL_DIVIDER) == 0) {
        /* GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_1); */
        UARTprintf("Left pressed\n");
        send_message("left\0", 5);
      }
      break;

    case RIGHT_BUTTON:
      if ((systick_count % BUTTON_POLL_DIVIDER) == 0) {
        /* GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, 0); */
        UARTprintf("Right pressed\n");
        send_message("right\0", 6);
      }
      break;

    case ALL_BUTTONS:
      if ((systick_count % BUTTON_POLL_DIVIDER) == 0) {
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, 0);
        UARTprintf("Both pressed\n");
        send_message("bothpr\0", 7);
      }
      break;
  }
}

unsigned long transmit_handler(void* internal, unsigned long event,
                               unsigned long msg_value, void* msg_data) {
  switch (event) {
    case USB_EVENT_TX_COMPLETE: {
      unsigned char *e = (unsigned char*)msg_data;
      UARTprintf("[%d] MSGDATA: %d %d %d %d %d\n", msg_value, e[0], e[1], e[2], e[3], e[4]);
      return msg_value;
    }
  }

  UARTprintf("transmit complete\n");
  return 0;
}

static unsigned long process_rx_msg(tUSBDBulkDevice *device,
                                    unsigned long msg_length,
                                    unsigned char *msg_data) {
  UARTprintf("received %d bytes\n", msg_length);
  unsigned long read_index = (unsigned long)(msg_data - usb_receive_ring_buffer);

  UARTprintf("len: %d\n", msg_length);
  unsigned long unread_count = msg_length;
  while (unread_count) {
    GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_1);
    UARTprintf("\nGot %d\n", (usb_receive_ring_buffer[read_index] & 0xFF));
    SysCtlDelay(10000);
    GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, 0);
    SysCtlDelay(10000);

    input_buffer[input_buffer_limit++] = usb_receive_ring_buffer[read_index];

    --unread_count;
    ++read_index;
  }
  
  input_buffer[input_buffer_limit] = '\0';
  unsigned long command_status = CmdLineProcess(input_buffer);
  input_buffer_limit = 0;
  UARTprintf("cmd %s, %d\n", input_buffer, command_status);
  /* change */
  return msg_length;
}

unsigned long receive_handler(void* internal, unsigned long event,
                              unsigned long msg_value, void* msg_data) {
  switch (event) {
    case USB_EVENT_CONNECTED: {
      usb_configured = true;
      UARTprintf("connected\n");
      USBBufferFlush(&usb_transmit_buffer);
      USBBufferFlush(&usb_receive_buffer);
      break;
    }

    case USB_EVENT_DISCONNECTED: {
      usb_configured = false;
      UARTprintf("disconnected\n");
      break;
    }

    case USB_EVENT_RX_AVAILABLE: {
      tUSBDBulkDevice *device = (tUSBDBulkDevice*)internal;
      return process_rx_msg(device, msg_value, msg_data);
    }

    case USB_EVENT_SUSPEND:
      /* TODO */
      break;

    case USB_EVENT_RESUME:
      /* TODO */
      break;

    default:
      break;
  }
  return 0;
}

#define ENABLE_OUTPUT(port, pins) ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIO##port); \
                                  ROM_GPIOPinTypeGPIOOutput(GPIO_PORT##port##_BASE, pins);

int main(void) {
  ROM_FPULazyStackingEnable();
  
  init_clock_50mhz();

  input_buffer_limit = 0;

  /* usb pins */
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
  ROM_GPIOPinTypeUSBAnalog(GPIO_PORTD_BASE, GPIO_PIN_5 | GPIO_PIN_4);


  ENABLE_OUTPUT(A, PORTA_PIN);
  ENABLE_OUTPUT(B, PORTB_PIN);
  ENABLE_OUTPUT(C, PORTC_PIN);
  ENABLE_OUTPUT(D, PORTD_PIN);
  ENABLE_OUTPUT(E, PORTE_PIN);
  ENABLE_OUTPUT(F, PORTF_PIN);
  
  usb_configured = false;

  /* init_uart0(); */
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
  GPIOPinConfigure(GPIO_PA0_U0RX);
  GPIOPinConfigure(GPIO_PA1_U0TX);
  ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
  
  ROM_SysTickPeriodSet(ROM_SysCtlClockGet() / SYSTICKS_PER_SECOND);
  ROM_SysTickIntEnable();
  ROM_SysTickEnable();

  UARTStdioInit(0);
  UARTprintf("Enabled debug output\n");

  // init usb transmit, receive buffer
  USBBufferInit((tUSBBuffer*)&usb_transmit_buffer);
  USBBufferInit((tUSBBuffer*)&usb_receive_buffer);

  /* device mode, vbus monitoring */
  USBStackModeSet(0, USB_MODE_DEVICE, 0);

  USBDBulkInit(0, (tUSBDBulkDevice*)&usb_bulk_device);

  ButtonsInit();
  
  while(1) {
  }
}
