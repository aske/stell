#include "inc/hw_types.h"
#include "driverlib/usb.h"
#include "usblib/usblib.h"
#include "usblib/usbcdc.h"
#include "usblib/usb-ids.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdbulk.h"
#include "usb_structs.h"

#include "inc/hw_memmap.h"
#include "usblib/usblibpriv.h"
#include "driverlib/debug.h"
#include "utils/uartstdio.h"

#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"

const unsigned char language_descriptor[] = {
  4,
  USB_DTYPE_STRING,
  USBShort(USB_LANG_EN_US)
};

const unsigned char manufacturer_string[] = {
  (17 + 1) * 2,
  USB_DTYPE_STRING,
  'T', 0, 'e', 0, 'x', 0, 'a', 0, 's', 0, ' ', 0, 'I', 0, 'n', 0, 's', 0,
  't', 0, 'r', 0, 'u', 0, 'm', 0, 'e', 0, 'n', 0, 't', 0, 's', 0,
};

const unsigned char product_string[] = {
  2 + (24 * 2),
  USB_DTYPE_STRING,
  'B', 0, 'M', 0, 'S', 0, 'T', 0, 'U', 0, ' ', 0,
  's', 0, 'y', 0, 's', 0, 'p', 0, 'r', 0, 'o', 0, 'g', 0, ' ', 0,
  'c', 0, 'o', 0, 'u', 0, 'r', 0, 's', 0, 'e', 0, 'w', 0, 'o', 0, 'r', 0, 'k'
};

const unsigned char serial_number_string[] = {
  2 + (8 * 2),
  USB_DTYPE_STRING,
  '1', 0, '2', 0, '3', 0, '4', 0, '5', 0, '6', 0, '7', 0, '8', 0
};


const unsigned char data_interface_string[] = {
  (19 + 1) * 2,
  USB_DTYPE_STRING,
  'B', 0, 'u', 0, 'l', 0, 'k', 0, ' ', 0, 'D', 0, 'a', 0, 't', 0,
  'a', 0, ' ', 0, 'I', 0, 'n', 0, 't', 0, 'e', 0, 'r', 0, 'f', 0,
  'a', 0, 'c', 0, 'e', 0
};

const unsigned char config_string[] = {
  (23 + 1) * 2,
  USB_DTYPE_STRING,
  'B', 0, 'u', 0, 'l', 0, 'k', 0, ' ', 0, 'D', 0, 'a', 0, 't', 0,
  'a', 0, ' ', 0, 'C', 0, 'o', 0, 'n', 0, 'f', 0, 'i', 0, 'g', 0,
  'u', 0, 'r', 0, 'a', 0, 't', 0, 'i', 0, 'o', 0, 'n', 0
};

const unsigned char * const string_descriptors[] = {
  language_descriptor,
  manufacturer_string,
  product_string,
  serial_number_string,
  data_interface_string,
  config_string,
};

#define NUM_STRING_DESCRIPTORS (sizeof(string_descriptors) /    \
                                sizeof(unsigned char *))

tBulkInstance bulk_instance;

extern const tUSBBuffer usb_transmit_buffer;
extern const tUSBBuffer usb_receive_buffer;

#define PRODUCT_ID 0x1234

const tUSBDBulkDevice usb_bulk_device = {
  USB_VID_STELLARIS,
  PRODUCT_ID,
  500,
  USB_CONF_ATTR_BUS_PWR,
  USBBufferEventCallback,
  (void*)&usb_receive_buffer,
  USBBufferEventCallback,
  (void*)&usb_transmit_buffer,
  string_descriptors,
  NUM_STRING_DESCRIPTORS,
  &bulk_instance,
};

/* from usbdbulk */
static void SetDeferredOpFlag(volatile unsigned short *pusDeferredOp,
                              unsigned short usBit, tBoolean bSet) {
  //
  // Set the flag bit to 1 or 0 using a bitband access.
  //
  HWREGBITH(pusDeferredOp, usBit) = bSet ? 1 : 0;
}

/* from usbdbulk */
unsigned long USBDBulkPacketReadDebug(void *pvInstance, unsigned char *pcData,
                                      unsigned long ulLength, tBoolean bLast) {
  unsigned long ulEPStatus, ulCount, ulPkt;
  tBulkInstance *psInst;
  long lRetcode;

  ASSERT(pvInstance);

  //
  // Get our instance data pointer
  //
  psInst = ((tUSBDBulkDevice *)pvInstance)->psPrivateBulkData;

  //
  // Does the relevant endpoint FIFO have a packet waiting for us?
  //
  ulEPStatus = USBEndpointStatus(psInst->ulUSBBase,
                                 psInst->ucOUTEndpoint);

  if(ulEPStatus & USB_DEV_RX_PKT_RDY)
    {

      
      //
      // How many bytes are available for us to receive?
      //
      ulPkt = USBEndpointDataAvail(psInst->ulUSBBase,
                                   psInst->ucOUTEndpoint);

      //
      // Get as much data as we can.
      //
      ulCount = ulLength;
      /* lRetcode = USBEndpointDataGet(psInst->ulUSBBase, */
      /*                                   psInst->ucOUTEndpoint, */
      /*                                   pcData, &ulCount); */

      lRetcode = USBEndpointDataGet(psInst->ulUSBBase,
                                    psInst->ucOUTEndpoint,
                                    pcData,
                                    &ulCount);

        
      UARTprintf("\n\ndata[res=%d][%d] %u %u %u\n", lRetcode, ulCount, pcData[0], pcData[1], pcData[ulCount-1]);
      GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_2);
      SysCtlDelay(40000);

      //
      // Did we read the last of the packet data?
      //
      if(ulCount == ulPkt)
        {
          //
          // Clear the endpoint status so that we know no packet is
          // waiting.
          //
          USBDevEndpointStatusClear(psInst->ulUSBBase,
                                    psInst->ucOUTEndpoint,
                                    ulEPStatus);

          //
          // Acknowledge the data, thus freeing the host to send the
          // next packet.
          //
          USBDevEndpointDataAck(psInst->ulUSBBase, psInst->ucOUTEndpoint,
                                true);

          //
          // Clear the flag we set to indicate that a packet read is
          // pending.
          //
          /* PACKET_DO_RX = 5 */
          SetDeferredOpFlag(&psInst->usDeferredOpFlags, 5,
                            false);
        }

      //
      // If all went well, tell the caller how many bytes they got.
      //
      if(lRetcode != -1)
        {
          return(ulCount);
        }
    }

  //
  // No packet was available or an error occurred while reading so tell
  // the caller no bytes were returned.
  //
  return(0);
}

unsigned char usb_receive_ring_buffer[BULK_BUFFER_SIZE];
unsigned char usb_receive_workspace[USB_BUFFER_WORKSPACE_SIZE];
const tUSBBuffer usb_receive_buffer = {
  false,
  receive_handler,                      
  (void*)&usb_bulk_device,        
  USBDBulkPacketRead,        
  USBDBulkRxPacketAvailable,   
  (void*)&usb_bulk_device,     
  usb_receive_ring_buffer,          
  BULK_BUFFER_SIZE,
  usb_receive_workspace,
};

unsigned char usb_transmit_ring_buffer[BULK_BUFFER_SIZE];
unsigned char usb_transmit_workspace[USB_BUFFER_WORKSPACE_SIZE];
const tUSBBuffer usb_transmit_buffer = {
  true,                        
  transmit_handler,                  
  (void*)&usb_bulk_device,    
  USBDBulkPacketWrite,      
  USBDBulkTxPacketAvailable,
  (void*)&usb_bulk_device,
  usb_transmit_ring_buffer,
  BULK_BUFFER_SIZE,
  usb_transmit_workspace,
};
