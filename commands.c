#include "inc/hw_types.h"
#include "utils/ustdlib.h"
#include "utils/uartstdio.h"
#include "utils/cmdline.h"
#include "driverlib/uart.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "inc/hw_gpio.h"
#include "inc/hw_uart.h"
#include "inc/hw_memmap.h"

#include "commands.h"

tCmdLineEntry g_sCmdTable[] = {
  { "test", CMD_test, " : test" },
  { "led", CMD_led, " : Bang some leds" },
  { "gpio-set", CMD_gpio, " : GPIO controls" },
  { 0, 0, 0 }
};

#define GPIO_WRITE(port, pins, enable) if (value == 1) { \
                                         GPIOPinWrite(port, pins, enable); \
                                        } else if (value == 0) { \
                                          GPIOPinWrite(port, pins, 0); \
                                        }

/* gpio-set [port] [pin] [value] */
int CMD_gpio(int argc, char** argv) {
  if (argc == 4) {
    unsigned long pin = ustrtoul(argv[2], 0, 10);
    unsigned long value = ustrtoul(argv[3], 0, 10);
    unsigned char pin_num;

    switch (pin) {
    case 0:
      pin_num = GPIO_PIN_0;
      break;

    case 1:
      pin_num = GPIO_PIN_1;
      break;
      
    case 2:
      pin_num = GPIO_PIN_2;
      break;
      
    case 3:
      pin_num = GPIO_PIN_3;
      break;
      
    case 4:
      pin_num = GPIO_PIN_4;
      break;
      
    case 5:
      pin_num = GPIO_PIN_5;
      break;
      
    case 6:
      pin_num = GPIO_PIN_6;
      break;
      
    case 7:
      pin_num = GPIO_PIN_7;
      break;
      
    }
    switch (argv[1][0]) {
      case 'a':
        GPIO_WRITE(GPIO_PORTA_BASE, PORTA_PIN, pin_num);
        break;
        
      case 'b':
        GPIO_WRITE(GPIO_PORTB_BASE, PORTB_PIN, pin_num);
        break;

      case 'c':
        GPIO_WRITE(GPIO_PORTC_BASE, PORTC_PIN, pin_num);
        break;

      case 'd':
        GPIO_WRITE(GPIO_PORTD_BASE, PORTD_PIN, pin_num);
        break;
        
      case 'e':
        GPIO_WRITE(GPIO_PORTE_BASE, PORTE_PIN, pin_num);
        break;

      case 'f':
        GPIO_WRITE(GPIO_PORTF_BASE, PORTF_PIN, pin_num);
        break;
    }
  }
  return 0;
}

const int NUM_CMD = sizeof(g_sCmdTable)/sizeof(tCmdLineEntry);

int CMD_test(int argc, char** argv) {
  if (argc == 2) {
    UARTprintf("Got message: %s", argv[1]);
  }
  return 0;
}

int CMD_led(int argc, char** argv) {
  unsigned long led_num;
  
  if (argc == 2) {
    led_num = ustrtoul(argv[1], 0, 10);
    switch (led_num) {
    case 1:
      GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_1);
      break;

    case 2:
      GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_2);
      break;
      
    case 3:
      GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_PIN_3);
      break;
    };
  }
  return 0;
}
