#ifndef UTILITIES_H
#define UTILITIES_H

void init_uart0(void);
void init_clock_40mhz(void);
void init_clock_50mhz(void);
void wait_command(void);

#endif
