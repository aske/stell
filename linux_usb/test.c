#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main() {
  int fd = open("/dev/stell0", O_WRONLY);
  if (fd < 0) {
    fd = open("/dev/stell1", O_WRONLY);
  }
  printf("fd = %d\n", fd);

  printf("input: ");
  char str[64];
  fgets(str, 64, stdin);

  int written = write(fd, str, strlen(str));
  printf("written = %d\n", written);

  /* close(fd); */

  return 0;
}
