#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <linux/errno.h>
#include <linux/kref.h>
#include <linux/uaccess.h>
 
#define BULK_OUT_ENDPOINT 0x01
#define BULK_IN_ENDPOINT 0x81
#define MAX_PACKET_SIZE 64
#define READ_TIMEOUT 10000

struct usb_stell {
  struct usb_device *udev;
  struct usb_interface *interface;
  __u8 bulk_in_endpointAddr;
  __u8 bulk_out_endpointAddr;
  int errors;
  spinlock_t err_lock;
  struct kref kref;
  struct mutex io_mutex;
  struct usb_anchor in_anchor;
};

static struct usb_driver stell_driver;

static int stell_open(struct inode *inode, struct file *file) {
  int subminor = iminor(inode);

  struct usb_interface *interface = usb_find_interface(&stell_driver, subminor);
  if (!interface) {
    pr_err("%s - error, can't find device for minor %d\n", __func__, subminor);
    return -ENODEV;
  }

  struct usb_stell *dev = usb_get_intfdata(interface);
  if (!dev) {
    return -ENODEV;
  }

  kref_get(&dev->kref);
  file->private_data = dev;
  return nonseekable_open(inode, file);
}

static int stell_close(struct inode *inode, struct file *file) {
  printk("CLOSE\n");
  return 0;
}

static void stell_delete(struct kref *kref) {
  struct usb_stell *dev = container_of(kref, struct usb_stell, kref);
  printk(KERN_INFO "BBBBBBBBBB\n");
  usb_put_dev(dev->udev);
  kfree(dev);
}

static void stell_read_bulk_callback(struct urb *urb) {
  printk("READ CALLBACK\n");
  struct usb_stell *dev = urb->context;

  unsigned long flags;
  if (urb->status) {
    if (!(urb->status == -ENOENT ||
          urb->status == -ECONNRESET ||
          urb->status == -ESHUTDOWN)) {
      dev_err(&dev->interface->dev,
              "%s - nonzero write bulk status received: %d\n",
              __func__, urb->status);
    }
    spin_lock_irqsave(&dev->err_lock, flags);
    dev->errors = urb->status;
    spin_unlock_irqrestore(&dev->err_lock, flags);
  }

  kfree(urb->transfer_buffer);
  printk(KERN_INFO "end read callback\n");
}

static ssize_t stell_read(struct file *file, char __user *buffer, size_t count, loff_t *ppos) {
  printk("Rread\n");
  printk("Want to read %zu!\n", count);
  
  /* size_t read_size = min(count, (size_t)MAX_PACKET_SIZE); */
  size_t read_size = count;
  
  struct usb_stell *dev = file->private_data;
  if (count == 0) {
    return 0;
  }
  
  int rv = 0;
  if (!dev->interface) {
    return -ENODEV;
  }

  unsigned long flags;
  spin_lock_irqsave(&dev->err_lock, flags);
  rv = dev->errors;
  if (rv) {
    dev->errors = 0;
  }
  spin_unlock_irqrestore(&dev->err_lock, flags);
  rv = (rv == -EPIPE) ? -EIO : rv;
  if (rv) goto error;
  unsigned timeout;

  struct urb *urb = usb_alloc_urb(0, GFP_KERNEL);
  if (!urb) {
    return -ENOMEM;
  }

  char *transfer_buffer = kmalloc(read_size, GFP_KERNEL);
  if (!transfer_buffer) {
    rv = -ENOMEM;
    goto error;
  }

  usb_fill_bulk_urb(urb, dev->udev, 
                    usb_rcvbulkpipe(dev->udev, dev->bulk_in_endpointAddr),
                    transfer_buffer, read_size,
                    stell_read_bulk_callback, dev);
  usb_anchor_urb(urb, &dev->in_anchor);

  rv = usb_submit_urb(urb, GFP_KERNEL);
  timeout = usb_wait_anchor_empty_timeout(&dev->in_anchor, READ_TIMEOUT);
  if (!timeout) {
    usb_kill_urb(urb);
    return -ETIMEDOUT;
  } else {
    rv = dev->errors == -EPIPE ? -EIO : dev->errors;
  }
  
  printk("Wait end\n");
  size_t bytes_filled = urb->actual_length;
  if (copy_to_user(buffer, transfer_buffer, bytes_filled)) {
    return -EFAULT;
  } else {
    return bytes_filled;
  }
  
  usb_free_urb(urb);
  
error:
  /* mutex_unlock(&dev->io_mutex); */
  return rv;
}

static void stell_write_bulk_callback(struct urb *urb) {
  struct usb_stell *dev = urb->context;

  unsigned long flags;
  if (urb->status) {
    if (!(urb->status == -ENOENT ||
          urb->status == -ECONNRESET ||
          urb->status == -ESHUTDOWN)) {
      dev_err(&dev->interface->dev,
              "%s - nonzero write bulk status received: %d\n",
              __func__, urb->status);
    }
    spin_lock_irqsave(&dev->err_lock, flags);
    dev->errors = urb->status;
    spin_unlock_irqrestore(&dev->err_lock, flags);
  }

  kfree(urb->transfer_buffer);
  printk(KERN_INFO "end write callback\n");
}

static ssize_t stell_write(struct file *file, const char __user *user_buffer,
                          size_t count, loff_t *ppos) {
  printk(KERN_INFO "write\n");
  
  int retval = 0;
  size_t write_size = min(count, (size_t)MAX_PACKET_SIZE);
  struct usb_stell *dev = file->private_data;

  if (count == 0) {
    return 0;
  }

  unsigned long flags;
  spin_lock_irqsave(&dev->err_lock, flags);
  retval = dev->errors;
  if (retval < 0) {
    dev->errors = 0;
    retval = (retval == -EPIPE) ? retval : -EIO;
  }
  spin_unlock_irqrestore(&dev->err_lock, flags);
  if (retval < 0) {
    return retval;
  }

  struct urb *urb = usb_alloc_urb(0, GFP_KERNEL);
  if (!urb) {
    return -ENOMEM;
  }

  char *transfer_buffer = kmalloc(write_size, GFP_KERNEL);
  if (!transfer_buffer) {
    retval = -ENOMEM;
    goto error;
  }

  if (copy_from_user(transfer_buffer, user_buffer, write_size)) {
    retval = -EFAULT;
    goto error;
  }

  mutex_lock(&dev->io_mutex);
  if (!dev->interface) { /* disconnect() was called */
    mutex_unlock(&dev->io_mutex);
    retval = -ENODEV;
    goto error;
  }

  usb_fill_bulk_urb(urb, dev->udev,
                    usb_sndbulkpipe(dev->udev, dev->bulk_out_endpointAddr),
                    transfer_buffer, write_size,
                    stell_write_bulk_callback, dev);

  retval = usb_submit_urb(urb, GFP_KERNEL);
  mutex_unlock(&dev->io_mutex);
  if (retval) {
    dev_err(&dev->interface->dev,
            "%s - failed submitting write urb, error %d\n",
            __func__, retval);
    goto error;
  }

  usb_free_urb(urb);
  
error:
    
  return retval;
}
static int stell_release(struct inode *inode, struct file *file) {
  struct usb_stell *dev = file->private_data;
  if (dev == NULL) {
    return -ENODEV;
  }
  printk(KERN_INFO "stell_realease KKKK\n");

  /* allow autosuspend */
  /* mutex_lock(&dev->io_mutex); */
  /* if (dev->interface) { */
  /*   /\* usb_autopm_put_interface(dev->interface); *\/ */
  /* } */
  /* mutex_unlock(&dev->io_mutex); */
  usb_kill_anchored_urbs(&dev->in_anchor);

  kref_put(&dev->kref, stell_delete);
  return 0;
}

/* static int stell_flush(struct file *file, fl_owner_t id) { */
/*   struct usb_stell *dev = file->private_data; */
/*   if (dev == NULL) { */
/*     return -ENODEV; */
/*   } */

/*   mutex_lock */
/*   int time = usb_wait_anchor_empty_timeout(&dev->submitted, 1000); */
/*   if (!time) { */
/*     usb_kill_anchored_urbs(&dev->submitted); */
/*   } */
/*   usb_kill_urb(dev->bulk_in_urb); */
/* } */

static const struct file_operations stell_ops = {
  .owner = THIS_MODULE,
  .read = stell_read,
  .write = stell_write,
  .open = stell_open,
  .release = stell_release,
  .llseek = no_llseek,
  /* .llseek = noop_llseek, */
  /* .flush = stell_flush, */
};
 
static struct usb_class_driver stell_class = {
  .name = "stell%d",
  .fops = &stell_ops,
};

static int stell_probe(struct usb_interface *interface, const struct usb_device_id *id) {
  int retval = -ENOMEM;
  struct usb_stell *dev = kzalloc(sizeof(*dev), GFP_KERNEL);
  if (!dev) {
    dev_err(&interface->dev, "Out of memory\n");
    return -ENOMEM;
  }
  
  kref_init(&dev->kref);
  mutex_init(&dev->io_mutex);
  spin_lock_init(&dev->err_lock);
  init_usb_anchor(&dev->in_anchor);
  
  dev->udev = usb_get_dev(interface_to_usbdev(interface));
  dev->interface = interface;

  struct usb_host_interface *iface_desc = interface->cur_altsetting;
  for (int i = 0; i < iface_desc->desc.bNumEndpoints; ++i) {
    struct usb_endpoint_descriptor *endpoint = &iface_desc->endpoint[i].desc;

    if (!dev->bulk_out_endpointAddr && usb_endpoint_is_bulk_out(endpoint)) {
      /* found bulk OUT endpoint */
      /* TODO: more init */
      dev->bulk_out_endpointAddr = endpoint->bEndpointAddress;
    } else if (!dev->bulk_in_endpointAddr && usb_endpoint_is_bulk_in(endpoint)) {
      /* found bulk IN endpoint */
      dev->bulk_in_endpointAddr = endpoint->bEndpointAddress;
    }
  }

  if (!(dev->bulk_in_endpointAddr && dev->bulk_out_endpointAddr)) {
    dev_err(&interface->dev, "Couldn't find both bulk IN and OUT endpoints\n");
    goto error;
  }

  if (!dev->bulk_out_endpointAddr) {
    dev_err(&interface->dev, "Couldn't fund bulk OUT endpoint\n");
    goto error;
  }

  usb_set_intfdata(interface, dev);

  /* device is ready, registering */
  retval = usb_register_dev(interface, &stell_class);
  if (retval) {
    dev_err(&interface->dev, "Not able to get minor for this device\n");
    usb_set_intfdata(interface, NULL);
    goto error;
  }

  dev_info(&interface->dev, "stell device now attached to stell%d", interface->minor);

  printk(KERN_INFO "SSSS: end probe\n");
  return 0;
  
error:
  if (dev) {
    kref_put(&dev->kref, stell_delete);
  }
  return retval;
}
 
static void stell_disconnect(struct usb_interface *interface) {
  struct usb_stell *dev = usb_get_intfdata(interface);
  int minor = interface->minor;

  usb_deregister_dev(interface, &stell_class);

  mutex_lock(&dev->io_mutex);
  dev->interface = NULL;
  mutex_unlock(&dev->io_mutex);

  usb_kill_anchored_urbs(&dev->in_anchor);

  kref_put(&dev->kref, stell_delete);

  dev_info(&interface->dev, "stell device #%d now disconnected", minor);
}
 
static struct usb_device_id stell_table[] = {
  { USB_DEVICE(0x1cbe, 0x1234) },
  {}
};
MODULE_DEVICE_TABLE (usb, stell_table);
 
static struct usb_driver stell_driver = {
  .name = "stell_driver",
  .probe = stell_probe,
  .disconnect = stell_disconnect,
  .id_table = stell_table,
};

module_usb_driver(stell_driver);

MODULE_AUTHOR("Shlomo shlomovich");
MODULE_LICENSE("GPL");
