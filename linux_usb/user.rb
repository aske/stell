#!/usr/bin/ruby
# -*- coding: utf-8 -*-

puts "Программа управление Stellaris Launchpad"

threads = []

threads << Thread.new do
  loop do
    output = `./read`
    case output
     when "Left" then puts "Была нажата левая кнопка"
     when "Right" then puts "Была нажата правая кнопка"
     when "Both" then puts "Были нажаты обе кнопки"
    end
  end
end

def command
  puts "Введите команду (gpio-set [порт] [контакт] [1|0])"
  out = `./write`
  puts out
end

threads << Thread.new do
  loop do
  puts "Введите 'c' для ввода команды"
  print "> "
  input = gets
  case input.chomp
    when 'c' then command()
    # when 'q' then terminate()
  end
  end
end

threads.each { |t| t.join }
