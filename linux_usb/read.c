#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main() {
  int fd = open("/dev/stell0", O_RDONLY);
  if (fd < 0) {
    fd = open("/dev/stell1", O_RDONLY);
  }

  char str[64];

  int rd = read(fd, &str, 7);
  switch (rd) {
  case 7:
    printf("Both");
    return 7;
    break;
  case 5:
    printf("Left");
    return 5;
    break;
  case 6:
    printf("Right");
    return 6;
    break;
  }
  
  close(fd);

  return 0;
}
